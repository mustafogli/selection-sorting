package com.mustafogli;


public class Main {

    public static void main(String[] args) {
	    int[] arr = {4,2,8,11,7,1};
        for (int items : arr){
            System.out.print(items+" ");
        }
        for (int i=0; i<arr.length - 1; i++){
            int minIndex = i;
            for (int j = i+1; j<arr.length; j++){
                if (arr[j] < arr[minIndex]){
                    minIndex = j;
                }
            }
            int temp = arr[minIndex];
            arr[minIndex] = arr[i];
            arr[i] = temp;
        }
        System.out.println("\n");
        for (int item : arr){
            System.out.print(item+" ");
        }
    }
}
